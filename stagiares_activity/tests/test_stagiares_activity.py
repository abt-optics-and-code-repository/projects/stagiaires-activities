"""
High-level tests for the  package.

"""

import stagiares_activity


def test_version():
    assert stagiares_activity.__version__ is not None
